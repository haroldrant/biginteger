/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class BigInteger_UFPS {

    /**
     * Numero="23456" miNUmero={2,3,4,5,6};
     */
    private int miNumero[];

    public BigInteger_UFPS() {
    }

    public BigInteger_UFPS(String miNumero) {
        String[] split = miNumero.split("");
        this.miNumero = new int[split.length];

        for (int i = 0; i < this.miNumero.length; i++) {
            this.miNumero[i] = Integer.parseInt(split[i]);
        }
    }

    public int[] getMiNumero() {
        return miNumero;
    }

    /**
     * Calcula los ceros a la derecha de una matriz.
     * Ej: String[] matriz = new String("1561","19689","98762");
     * matriz[0] ---> 0 ceros
     * matriz[1] ---> 1 cero
     * matriz[2] ---> 2 ceros     * 
     * @param r matriz de BigInteger_UFPS
     */
    private void calcularExponente(BigInteger_UFPS[] r) {
        for (int i = 1; i < r.length; i++) {
            int base = 10, resultado = 1;
            int j = i;
            while (j > 0) {
                resultado = base * resultado;
                j--;
            }
            String[] ceros = String.valueOf(resultado).split("");
            for (int k = 1; k < ceros.length; k++) {
                r[i] = new BigInteger_UFPS(r[i] + ceros[k]);
            }
        }
    }

    /**
     * Mutiplica dos enteros BigInteger
     *
     * @param dos
     * @return
     */
    public BigInteger_UFPS multiply(BigInteger_UFPS dos) {
        int resultado, contador = 0;        
        BigInteger_UFPS result[] = new BigInteger_UFPS[dos.getMiNumero().length];
        for (int i = dos.getMiNumero().length - 1; i >= 0; i--, contador++) {
            String resultadoBig = "";
            int resto = 0;
            for (int j = this.getMiNumero().length - 1; j >= 0; j--) {
                resultado = (this.getMiNumero()[j] * dos.getMiNumero()[i]) + resto;
                resto = 0;
                if (resultado > 9 && j != 0) {
                    resto = resultado / 10;
                    resultado %= 10;
                }
                resultadoBig = resultado + resultadoBig;
            }
            result[contador] = new BigInteger_UFPS(resultadoBig);
        }
        if (result.length > 1) {
            return sumarMatrices(result);
        }
        return result[0];
    }
    
    /**
     * Suma los resultados de la multiplicación cuando se multiplica por dos o más digitos.
     * Recibe como parametro un arreglo de bigInteger_UFPS.
     * @param big matriz de BigInteger_UFPS
     * @return un BigInteger_UFPS.
     */
    private BigInteger_UFPS sumarMatrices(BigInteger_UFPS[] big) {
        calcularExponente(big);
        int resultado = 0;
        String resultadoBig = "";
        for (int i = 1; i < big.length; i++) {
            int j = big[0].getMiNumero().length - 1, k = big[i].getMiNumero().length - 1;
            int resto = 0;
            for (; j >= 0 && k >= 0; j--, k--) {
                resultado = big[0].getMiNumero()[j] + big[i].getMiNumero()[k] + resto;
                resto = 0;
                if (resultado > 9 && (j != 0 || k != 0)) {
                    resto = resultado / 10;
                    resultado %= 10;
                }
                resultadoBig = resultado + resultadoBig;
            }
            if (j != -1) {
                resultadoBig = sumarDesiguales(j, resultadoBig, resto, big[0].getMiNumero());
            } else {
                if (k != -1) {
                    resultadoBig = sumarDesiguales(k, resultadoBig, resto, big[i].getMiNumero());
                }
            }
            big[0] = new BigInteger_UFPS(resultadoBig);
            resultadoBig = "";
        }
        return big[0];
    }
    
    /**
     * Realiza la última parte de la suma cuando son números de distinto tamaño.
     * Recibe como parámetro la variable contador, el string con el resultado hasta el momento, la variable que lleva
     * y el arreglo de BigInteger_UFPS expresado como arreglo de enteros.
     * @param aux contador de la matriz
     * @param resultadoBig resultado hasta el momento expresado en un String
     * @param resto valor que llevaba de la suma anterior
     * @param  big matriz de enteros que representa un BigIntege_UFPS en su variable miNumero.
     * @return String.
     */
    private String sumarDesiguales(int aux, String resultadoBig, int resto, int[] big) {
        int resultado = 0;
        while (aux > -1) {
            resultado = big[aux] + resto;
            resto = 0;
            if (resultado > 9 && aux != 0) {
                resto = resultado / 10;
                resultado %= 10;
            }
            resultadoBig = resultado + resultadoBig;
            aux--;
        }
        return resultadoBig;
    }

    /**
     * Retorna la representación entera del BigInteger_UFPS
     *
     * @return un entero
     */
    public int intValue() {
        int valor = this.miNumero[0];

        for (int i = 1; i < this.miNumero.length; i++) {
            valor = (valor * 10) + this.miNumero[i];
        }

        return valor;
    }

    @Override
    public String toString() {
        String valor = "";

        for (int n : this.miNumero) {
            valor += n;
        }

        return valor;
    }

}
